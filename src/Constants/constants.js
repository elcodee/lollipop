/*
   Copyright (C), 2023-2024, Sara Echeverria (bl33h)
   Author: Sara Echeverria
   FileName: constants.js
   Version: I
   Creation: 02/06/2023
   Last modification: 18/12/2023
*/

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faX,
  faBars,
  faWindowRestore,
  faBagShopping,
  faDiceD6,
  faEnvelope,
} from '@fortawesome/free-solid-svg-icons';
import {
  faReact,
  faGithub,
  faLinkedin,
} from '@fortawesome/free-brands-svg-icons';

import {
  htmlIcon,
  cssIcon,
  jsIcon,
  reactIcon,
  awsIcon,
  javaIcon,
  gitIcon,
  githubIcon,
  psqlIcon,
  eslintIcon,
  pyIcon,
  viteIcon,
  nodeIcon,
  raspIcon,
  neoIcon,
  figmaIcon,
  pawsitivePrototype,
  calculator,
  memoryGame,
  avatar,
} from '../assets';

library.add(faX, faBars, faWindowRestore, faBagShopping, faDiceD6);

const media = {
  htmlIcon,
  cssIcon,
  jsIcon,
  reactIcon,
  awsIcon,
  javaIcon,
  gitIcon,
  githubIcon,
  psqlIcon,
  eslintIcon,
  pyIcon,
  viteIcon,
  nodeIcon,
  raspIcon,
  neoIcon,
  figmaIcon,
  avatar,
};

const icons = {
  faBars,
  faX,
  faWindowRestore,
  faBagShopping,
  faDiceD6,
  faReact,
  faGithub,
  faLinkedin,
  faEnvelope,
};

const introduction = {
  text: [
    "Hallo peeps !",

		"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",

    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
  ],
};

export const navLinks = [
  {
    id: 'about',
    title: 'About',
  },
  // {
  //   id: 'projects',
  //   title: 'Projects',
  // },
  {
    id: 'skills',
    title: 'Members',
  },
  {
    id: 'contact',
    title: 'Find US',
  },
];

const projects = [
  {
    name: "Arasartara's Gallery",
    description: 'A memory game with an art theme implemented with React. Test your memory skills and enjoy beautiful artwork as you match pairs of cards in this engaging game.',
    image: memoryGame,
    source_code_link: 'https://github.com/bl33h/artMemoryGame',
    demo_link: 'https://arasarmemory.netlify.app/',
  },
  {
    name: 'Pawsitive',
    description: 'Explore a user-friendly Figma prototype for an adoption and sterilization awareness website. Experience the sleek design and contribute to a noble cause.',
    image: pawsitivePrototype,
    source_code_link: 'https://github.com/bl33h/pawsitive',
    demo_link: 'https://www.figma.com/proto/zhK8DMa7uPyQIcHxkemIcx/Pawsitive?type=design&node-id=40-2&scaling=min-zoom&page-id=0%3A1&starting-point-node-id=40%3A2',
  },
  {
    name: 'Basic Calculator',
    description: 'A completely functional basic calculator website created with React. It allows you to perform mathematical calculations effortlessly. With testing implemented, the results are accurate.',
    image: calculator,
    source_code_link: 'https://github.com/bl33h/calculator',
    demo_link: 'https://bl33hscalculator.netlify.app/',
  },
];

const memoji = {
  image: ['https://th.bing.com/th/id/OIG2.bVpewfxmzmQHrLAnK_6F?w=1024&h=1024&rs=1&pid=ImgDetMain', `https://th.bing.com/th/id/OIG1.dbkD9hqA9Hzlw9L4BWFu?w=1024&h=1024&rs=1&pid=ImgDetMain`, 'https://th.bing.com/th/id/OIG1.xdffoxa3hU5dGKCyaB_m?pid=ImgGn'],
};

const skills = [
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
  {
    id: 'html',
    title: 'Bécanē',
    icon: 'https://i.ibb.co/c895N8s/Whats-App-Image-2024-03-03-at-19-52-21-2223fee5.jpg',
    description:
      'u so plebeians',
  },
];

const markerSvg = `<svg viewBox="-4 0 36 36">
    <path fill="currentColor" d="M14,0 C21.732,0 28,5.641 28,12.6 C28,23.963 14,36 14,36 C14,36 0,24.064 0,12.6 C0,5.641 6.268,0 14,0 Z"></path>
    <circle fill="black" cx="14" cy="14" r="7"></circle>
  </svg>`;

export {
  media,
  introduction,
  projects,
  memoji,
  skills,
  markerSvg,
  icons,
};
